/***
 *  $Id$
 **
 *  File: parcel.cpp
 *  Created: Aug 03, 2011
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@gmail.com>
 *          Rahul Nihalani <rahulnih@gmail.com>
 */

#include <iostream>
#include <iterator>
#include <queue>
#include <set>
#include <stack>
#include <vector>

#include <jaz/algorithm_add.hpp>
#include <jaz/utility_add.hpp>

#include "Cluster.hpp"


namespace ext {

  template <typename T, class Container = std::deque<T> >
  struct queue : public std::queue<T, Container> {
      T& top() { return this->front(); }
      const T& top() const { return this->front(); }
  }; // class queue

} // namespace ext


struct Node {
    unsigned int id;
    std::vector<uint64_t> memset;
    std::vector<unsigned int> eindex;
    std::vector<Cluster::edge_type> edges;
}; // struct Node

struct is_unique_node {
    bool operator()(const Node& node) const { return (node.memset.size() < 2); }
}; // struct is_unique_node

inline bool operator<(const Node& lhs, const Node& rhs) {
    return (lhs.memset.size() < rhs.memset.size());
} // operator<

std::ostream& operator<<(std::ostream& os, const Node& node) {
    if (node.memset.empty() == false) {
	os << node.id << '\t' << node.memset[0];
	for (unsigned int i = 1; i < node.memset.size(); ++i) os << " " << node.memset[i];
	os << "\n";
	for (unsigned int i = 0; i < node.eindex.size(); ++i) os << " " << node.eindex[i];
	os << "\n";
	for (unsigned int i = 0; i < node.edges.size(); ++i) {
	    os << " " << node.edges[i].first << ":" << node.edges[i].second;
	}
    }
    return os;
} // operator<<


struct BBNode {
    unsigned int score;
    std::vector<unsigned int> alloc;
}; // struct BBNode

std::ostream& operator<<(std::ostream& os, const BBNode& node) {
    os << node.score << "\n";
    for (unsigned int i = 0; i < node.alloc.size(); ++i) os << " " << node.alloc[i];
    return os;
} // operator<<



class EdgeListUpdater {
public:
    EdgeListUpdater(const std::vector<Node>& nodes) : nodes_(nodes) { }

    /**
     *  See BBFunction for clarification of Iter, X and CX.
     */
    template <typename Iter>
    void update(Iter first, std::vector<unsigned int>& X, std::vector<unsigned int>& CX,
		std::vector<Cluster::edge_type>& L) const {
       	// get list of all nodes adjacent to nodes from X
	std::vector<unsigned int> V;

	L.clear();

	for (unsigned int i = 0; i < X.size(); ++i) {
	    unsigned int v = X[i];
	    unsigned int vid = nodes_[v].id;
	    unsigned int pos = first[v];
	    unsigned int j = nodes_[v].eindex[pos];
	    unsigned int k = nodes_[v].eindex[pos + 1];
	    for (; j < k; ++j) {
		unsigned int s = nodes_[v].edges[j].first;
		unsigned int t = nodes_[v].edges[j].second;
		if (s == vid) V.push_back(t); else V.push_back(s);
		L.push_back(nodes_[v].edges[j]);
	    } // for j
	} // for i

	// find nodes that should not be included in C
	std::sort(V.begin(), V.end());
	V.erase(std::unique(V.begin(), V.end()), V.end());

	std::sort(CX.begin(), CX.end());

	std::set<unsigned int> S;
	std::set_difference(V.begin(), V.end(), CX.begin(), CX.end(),
			    std::inserter(S, S.end()));

	// clean L accordingly
	std::sort(L.begin(), L.end());
	L.erase(std::unique(L.begin(), L.end()), L.end());
	L.erase(std::remove_if(L.begin(), L.end(), is_dangling(S)), L.end());
    } // update


private:
    struct is_dangling {
	is_dangling(const std::set<unsigned int>& S) : S_(S), end_(S_.end()) { }

	bool operator()(const Cluster::edge_type& e) const {
	    return ((S_.find(e.first) != end_) || (S_.find(e.second) != end_));
	} // operator()

	const std::set<unsigned int>& S_;
	std::set<unsigned int>::const_iterator end_;

    }; // struct is_dangling

    const std::vector<Node>& nodes_;

}; // class EdgeListUpdater


class BBFunction {
public:
    BBFunction(const std::vector<Node>& nodes, const std::vector<Cluster>& clusters)
	: nodes_(nodes), clusters_(clusters), elu_(nodes) { }

    /**
     *  Iter must be a RandomAccess iterator.
     *  [first, last) must store position in memset, i.e.
     *  for each node i \in [first, last) nodes[i].memset(first[i])
     *  gives id of the cluster to which i should be assigned to.
     */
    template <typename Iter>
    unsigned int operator()(unsigned int score, Iter first, Iter last) const {
	unsigned int n1 = last - first - 1;

	unsigned int Cpos = nodes_[n1].memset[first[n1]];

	// create set of positions of nodes that we want to include in C
	// except of the last node
	std::vector<unsigned int> X;

	for (unsigned int i = 0; i < n1; ++i) {
	    if (nodes_[i].memset[first[i]] == Cpos) X.push_back(i);
	}

 	// get list of nodes that will form updated cluster C
	std::vector<unsigned int> CX;

	clusters_[Cpos].nodes(std::back_inserter(CX));
	for (unsigned int i = 0; i < X.size(); ++i) CX.push_back(nodes_[X[i]].id);

	// get list of update edges
	std::vector<Cluster::edge_type> L;
	elu_.update(first, X, CX, L);

	// get current gamma (i.e. with the last node)
	unsigned int g = clusters_[Cpos].gamma(L.begin(), L.end());

	// get degree of the last node to add
	std::vector<unsigned int> V;

	unsigned int vid = nodes_[n1].id;

	unsigned int j = nodes_[n1].eindex[first[n1]];
	unsigned int k = nodes_[n1].eindex[first[n1] + 1];

	for (; j < k; ++j) {
	    unsigned int s = nodes_[n1].edges[j].first;
	    unsigned int t = nodes_[n1].edges[j].second;
	    if (s == vid) V.push_back(t); else V.push_back(s);
	}

	std::sort(V.begin(), V.end());
	unsigned int d = jaz::intersection_size(V.begin(), V.end(), CX.begin(), CX.end());

	// now all we need is new gamma
	unsigned int g_new = 0;

	if (d <= g) g_new = d;
	else {
	    // we have to compute g_new completely
	    X.push_back(n1);
	    CX.push_back(vid);
	    elu_.update(first, X, CX, L);
	    g_new = clusters_[Cpos].gamma(L.begin(), L.end());
	}

	return score + g_new - g - 1;
    } // operator()

private:
    const std::vector<Node>& nodes_;
    const std::vector<Cluster>& clusters_;

    EdgeListUpdater elu_;

}; // class BBFunction


unsigned int bb_gamma_tot(const std::vector<Cluster>& clusters) {
    unsigned int S = 0;
    Cluster::edge_type* null = 0;
    for (unsigned int i = 0; i < clusters.size(); ++i) {
	unsigned int s = clusters[i].gamma(null, null);
	S += s;
    } // for i
    return S;
} // bb_gamma_tot


void bb_ref_init_random(const std::vector<Node>& nodes, const std::vector<Cluster>& clusters,
			BBNode& ref) {
    unsigned int n = nodes.size();

    ref.score = n + bb_gamma_tot(clusters);
    ref.alloc.resize(n, 0);

    BBFunction F(nodes, clusters);

    for (unsigned int i = 0; i < n; ++i) {
	ref.score = F(ref.score, ref.alloc.begin(), ref.alloc.begin() + i + 1);
    } // for i
} // bb_ref_init_random

void bb_ref_init_greedy(const std::vector<Node>& nodes, const std::vector<Cluster>& clusters,
			BBNode& ref) {
    unsigned int n = nodes.size();

    ref.score = n + bb_gamma_tot(clusters);
    ref.alloc.resize(n, 0);

    for (unsigned int i = 0; i < n; ++i) {
	unsigned int pos = 1;
	unsigned int psz = nodes[i].eindex[1] - nodes[i].eindex[0];
	for (unsigned int j = 2; j < nodes[i].eindex.size(); ++j) {
	    unsigned int sz = nodes[i].eindex[j] - nodes[i].eindex[j - 1];
	    if (psz < sz) {
		psz = sz;
		pos = j;
	    }
	} // for j
	ref.alloc[i] = pos - 1;
    } // for i

    BBFunction F(nodes, clusters);

    for (unsigned int i = 0; i < n; ++i) {
	ref.score = F(ref.score, ref.alloc.begin(), ref.alloc.begin() + i + 1);
    } // for i
} // bb_ref_init_greedy


void shared_nodes(const std::vector<Cluster>& clusters, std::vector<Node>& nodes) {
    std::vector<unsigned int> nds;

    // create memset
    for (unsigned int i = 0; i < clusters.size(); ++i) {
	nds.clear();
	clusters[i].nodes(std::back_inserter(nds));
	for (unsigned int j = 0; j < nds.size(); ++j) {
	    if (nodes.size() <= nds[j]) nodes.resize(nds[j] + 1);
	    nodes[nds[j]].memset.push_back(clusters[i].id());
	} // for j
    } // for i

    for (unsigned int i = 0; i < nodes.size(); ++i) nodes[i].id = i;

    // remove unique nodes
    nodes.erase(std::remove_if(nodes.begin(), nodes.end(), is_unique_node()),
		nodes.end());
} // shared_nodes


void unique_clusters(std::vector<Node>& nodes, std::vector<Cluster>& clusters) {
    Cluster c;
    std::vector<Cluster>::iterator first, last;

    // extract edges
    for (unsigned int i = 0; i < nodes.size(); ++i) {
	unsigned int sz = nodes[i].memset.size();
	nodes[i].eindex.resize(sz + 1, 0);

	for (unsigned int j = 0; j < sz; ++j) {
	    // find cluster
	    c.id(nodes[i].memset[j]);
	    jaz::tie(first, last) = std::equal_range(clusters.begin(), clusters.end(), c);

	    unsigned int pos = first - clusters.begin();

	    // update node
	    nodes[i].memset[j] = pos;
	    clusters[pos].edges(nodes[i].id, std::back_inserter(nodes[i].edges));
	    nodes[i].eindex[j + 1] = nodes[i].edges.size();
	} // for j

    } // for i

    // clean clusters
    for (unsigned int i = 0; i < nodes.size(); ++i) {
	for (unsigned int j = 0; j < nodes[i].memset.size(); ++j) {
	    clusters[nodes[i].memset[j]].remove(nodes[i].id);
	} // for j
    } // for i

} // unique_clusters



void make_partitions(const BBNode& opt, const std::vector<Node>& nodes, std::vector<Cluster>& clusters) {
    EdgeListUpdater elu(nodes);

    std::vector<unsigned int> X;
    std::vector<unsigned int> CX;
    std::vector<Cluster::edge_type> L;

    for (unsigned int i = 0; i < clusters.size(); ++i) {
	X.clear();

	for (unsigned int j = 0; j < nodes.size(); ++j) {
	    if (nodes[j].memset[opt.alloc[j]] == i) X.push_back(j);
	}

	CX.clear();

	clusters[i].nodes(std::back_inserter(CX));
	for (unsigned int j = 0; j < X.size(); ++j) CX.push_back(nodes[X[j]].id);

	elu.update(opt.alloc.begin(), X, CX, L);
	clusters[i].extend(L.begin(), L.end());
    } // for i

} // make_partitions


int main(int argc, char* argv[]) {
    if (argc != 3) {
    std::cout << "Usage: " << argv[0] << " <in_dir> <out_filename>" << std::endl;
	return -1;
    }

    // read clusters
    std::cout << "reading clusters... " << std::flush;

    std::vector<Cluster> clusters;

    if ((read_clusters(argv[1], clusters) == false) || (clusters.empty() == true)) {
	std::cerr << "\nError: unable to read directory " << argv[1] << std::endl;
	return -1;
    }

    std::cout << clusters.size() << std::endl;

    // get shared nodes
    std::cout << "extracting shared nodes... " << std::flush;

    std::vector<Node> nodes;
    shared_nodes(clusters, nodes);

    std::cout << nodes.size() << std::endl;

    // remove clean clusters

    // currently we are just checking how many clusters
    // are "clean", i.e. do not contain shared nodes
    {
	std::cout << "identifying soft clusters... " << std::flush;

	std::vector<uint64_t> uc;

	for (unsigned int i = 0; i < nodes.size(); ++i) {
	    std::copy(nodes[i].memset.begin(), nodes[i].memset.end(),
		      std::back_inserter(uc));
	    // std::cerr << nodes[i].memset.size() << std::endl;
	} // for i

	std::sort(uc.begin(), uc.end());
	uc.erase(std::unique(uc.begin(), uc.end()), uc.end());

	std::cout << uc.size() << std::endl;
    }

    // prepare data
    std::cout << "extracting core clusters... " << std::flush;

    std::sort(clusters.begin(), clusters.end());
    std::sort(nodes.begin(), nodes.end());

    // create unique clusters
    // after this function is ok nodes store position of
    // memset clusters and not cluster ids!!!

    unique_clusters(nodes, clusters);

    std::cout << "ok\n";

    // *** main search part ***
    std::cout << "assigning nodes..." << std::endl;

    BBFunction F(nodes, clusters);
    unsigned int n = nodes.size();

    // current optimal solution
    BBNode opt;

    // bb_ref_init_random(nodes, clusters, opt);
    bb_ref_init_greedy(nodes, clusters, opt);

    std::cout << "initial solution: " << opt.score << std::endl;

#ifndef WITHOUT_BB
    BBNode q;
    std::stack<BBNode> Q;
    // ext::queue<BBNode> Q;

    q.score = nodes.size() + bb_gamma_tot(clusters);
    Q.push(q);

    unsigned int pos = 0;
    unsigned int score = 0;

    while (Q.empty() == false) {
	q = Q.top();
	Q.pop();

	pos = q.alloc.size();
	score = q.score;

	// create new search points
	q.alloc.resize(pos + 1);

	for (unsigned int i = 0; i < nodes[pos].memset.size(); ++i) {
	    q.alloc[pos] = i;
	    q.score = F(score, q.alloc.begin(), q.alloc.end());
	    if (opt.score < q.score) {
		if (q.alloc.size() == n) {
		    std::cout << "solution improved: " << q.score << std::endl;
		    opt = q;
		} else Q.push(q);
	    }
	} // for i

    } // while Q

    std::cout << "done!" << std::endl;
#endif // WITHOUT_BB

    // ***

    // here we go with writing output
    std::cout << "creating partitions... " << std::flush;

    make_partitions(opt, nodes, clusters);

    std::cout << "ok\n";
    std::cout << "writing result... " << std::flush;

    std::ofstream f(argv[2]);
    if (!f) {
	std::cout << "unable to create " << argv[2] << std::endl;
	return -1;
    }
    std::copy(clusters.begin(), clusters.end(), std::ostream_iterator<Cluster>(f, ""));
    f.close();

    std::cout << "ok" << std::endl;

    // std::copy(nodes.begin(), nodes.end(), std::ostream_iterator<Node>(std::cout, "\n"));
    // std::cout << "\n";
    // std::cout << opt << std::endl;

    return 0;
} // main
