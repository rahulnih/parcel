/***
 *  $Id$
 **
 *  File: algorithm_add.hpp
 *  Created: May 22, 2007
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@gmail.com>
 *  Copyright (c) 2004-2008 Jaroslaw Zola
 *  Distributed under the Boost Software License.
 *  See accompanying LICENSE.
 *
 *  This file is part of jaz.
 */

#ifndef JAZ_ALGORITHM_ADD_HPP
#define JAZ_ALGORITHM_ADD_HPP

#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <ostream>
#include <string>
#include <utility>
#include <vector>
#include <stdlib.h>


namespace jaz {

  /** Function to find smallest and largest element
   *  of the sequence [@a first, @a last).
   *  @param Iter must be a model of forward iterator.
   */
  template <typename Iter>
  std::pair<Iter, Iter> min_max_element(Iter first, Iter last) {
      std::pair<Iter, Iter> p(first, first);
      ++first;

      for (; first != last; ++first) {
	  if (*first < *p.first) p.first = first;
	  else if (*p.second < *first) p.second = first;
      }

      return p;
  } // min_max_element

  template <typename Iter, typename BinPred>
  std::pair<Iter, Iter> min_max_element(Iter first, Iter last, BinPred comp) {
      std::pair<Iter, Iter> p(first, first);
      ++first;

      for (; first != last; ++first) {
	  if (comp(*first, *p.first)) p.first = first;
	  else if (comp(*p.second, *first)) p.second = first;
      }

      return p;
  } // min_max_element


  /** Function to check if the sequence [@a first, @a last) is sorted.
   *  @param Iter must be a model of forward iterator.
   */
  template <typename Iter> bool is_sorted(Iter first, Iter last) {
      Iter next = first;
      ++next;
      for (; next != last; ++first, ++next) if (!(*first < *next)) return false;
      return true;
  } // is_sorted

  template <typename Iter, typename BinPred>
  bool is_sorted(Iter first, Iter last, BinPred comp) {
      Iter next = first;
      ++next;

      for (; next != last; ++first, ++next) {
	  if (comp(*first, *next) == false) return false;
      }

      return true;
  } // is_sorted


  /** Modulo n implementation of RandomNumberGenerator.
   *  @param T must be an integer type.
   *  @param n must be a non-negative value.
   *  @return number in the range [0, n).
   */
  template <typename T> T random(T n) { return rand() % n; }


  /**
   */
  template <typename InputIter, typename OutputIter>
  OutputIter random_sample_n(InputIter first, InputIter last, OutputIter out,
			     std::size_t n) {
      std::size_t r = std::distance(first, last);
      std::size_t m = std::min(n, r);

      while (m > 0) {
	  if (random(r) < m) {
	      *out = *first;
	      ++out;
	      --m;
	  }
	  --r;
	  ++first;
      }

      return out;
  } // random_sample_n


  /**
   */
  template <typename IterA, typename IterB, typename Comp>
  unsigned int intersection_size(IterA first_a, IterA last_a, IterB first_b, IterB last_b, Comp comp) {
      unsigned int S = 0;

      while ((first_a != last_a) && (first_b != last_b)) {
	  if (comp(*first_a, *first_b)) ++first_a;
	  else if (comp(*first_b, *first_a)) ++first_b;
	  else {
	      S++;
	      first_a++;
	      first_b++;
	  }
      }

      return S;
  } // intersection_size

  template <typename IterA, typename IterB>
  unsigned int intersection_size(IterA first_a, IterA last_a, IterB first_b, IterB last_b) {
      typedef typename std::iterator_traits<IterA>::value_type value_type;
      return intersection_size(first_a, last_a, first_b, last_b, std::less<value_type>());
  } // intersection_size


  /**
   */
  template <typename Iter, typename Pred>
  std::size_t count_unique(Iter first, Iter last, Pred pred) {
      std::size_t S = 1;
      Iter prev = first++;
      for (; first != last; ++first, ++prev) if (pred(*prev, *first) == false) ++S;
      return S;
  } // count_unique

  template <typename Iter>
  inline std::size_t count_unique(Iter first, Iter last) {
      typedef typename std::iterator_traits<Iter>::value_type value_type;
      return count_unique(first, last, std::equal_to<value_type>());
  } // count_unique

} // namespace jaz

#endif // JAZ_ALGORITHM_ADD_HPP
