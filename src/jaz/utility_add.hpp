/***
 *  $Id$
 **
 *  File: utility_add.hpp
 *  Created: Jun 18, 2009
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@gmail.com>
 *  Copyright (c) 2009 Jaroslaw Zola
 *  Distributed under the Boost Software License.
 *  See accompanying file LICENSE.
 *
 *  This file is part of jaz.
 */

#ifndef UTILITY_ADD_HPP
#define UTILITY_ADD_HPP

#include <iostream>
#include <utility>


namespace jaz {

  template <typename T1, typename T2>
  class tie_proxy__ {
  public:
      typedef T1 first_type;
      typedef T2 second_type;

      tie_proxy__(T1& t1, T2& t2) : first(t1), second(t2) { }

      template <typename U1, typename U2>
      void operator=(const std::pair<U1, U2>& p) {
	  first = p.first;
	  second = p.second;
      } // operator

      T1& first;
      T2& second;

  }; // class tie_proxy__

  /** This function mimics std::tr1::tie for std::pair.
   */
  template <typename T1, typename T2>
  inline tie_proxy__<T1, T2> tie(T1& t1, T2& t2) {
      return tie_proxy__<T1, T2>(t1, t2);
  } // tie


  /** This class is a copy of boost::scoped_array.
   */
  template <typename T> class scoped_array {
  public:
      typedef T element_type;

      explicit scoped_array(T* p = 0) : p_(p) { }

      ~scoped_array() { delete[] p_; }

      void swap(scoped_array& a) {
	  T* tmp = a.p_;
	  a.p_ = p_;
	  p_ = tmp;
      } // swap

      T* get() const { return p_; }

      void reset(T* p = 0) {
	  if ((p == 0) || (p != p_)) {
	      scoped_array(p).swap(*this);
	  }
      } // reset

      bool operator!() const { return p_ == 0; }

      T& operator[](unsigned int i) const { return p_[i]; }

  private:
      scoped_array(const scoped_array&);
      void operator=(const scoped_array&);

      T* p_;

  }; // class scoped_array

} // namespace jaz

#endif // UTILITY_ADD_HPP
