/***
 *  $Id$
 **
 *  File: Cluster.hpp
 *  Created: Aug 03, 2011
 *
 *  Author: Jaroslaw Zola <jaroslaw.zola@gmail.com>
 *          Rahul Nihalani <rahulnih@gmail.com>
 */

#ifndef CLUSTER_HPP
#define CLUSTER_HPP

#include <algorithm>
#include <fstream>
#include <vector>

#include <inttypes.h>

#include <jaz/string_add.hpp>
#include <jaz/sys_tools.hpp>


class Cluster {
public:
    typedef std::pair<unsigned int, unsigned int> edge_type;

    uint64_t id() const { return id_; }

    void id(uint64_t aid) { id_ = aid; }

    template <typename OutputIter> void nodes(OutputIter out) const {
	std::copy(nodes_.begin(), nodes_.end(), out);
    } // nodes

    template <typename OutputIter> void edges(unsigned int id, OutputIter out) {
	for (unsigned int i = 0; i < edges_.size(); ++i) {
	    if ((edges_[i].first == id) || (edges_[i].second == id)) {
		*(out++) = edges_[i];
	    }
	} // for i
    } // edges

    bool read(std::istream& is) {
	std::string buf;
	jaz::string_to st;

	std::getline(is, buf);
	if (!is) return false;

	std::vector<std::string> line;

	// get id
	jaz::split('\t', buf, std::back_inserter(line));
	if (line.size() != 2) return false;

	st(line[0], id_);

	// get edges
	std::vector<std::string> edges;
	jaz::split(' ', line[1], std::back_inserter(edges));

	edges_.resize(edges.size());
	for (unsigned int i = 0; i < edges_.size(); ++i) {
	    line.clear();

	    jaz::split(':', edges[i], back_inserter(line));
	    if (line.size() != 2) return false;

	    st(line[0], edges_[i].first);
	    st(line[1], edges_[i].second);

	    if (edges_[i].second < edges_[i].first) {
		std::swap(edges_[i].first, edges_[i].second);
	    }
	} // for i

	nodes_.clear();

	for (unsigned int i = 0; i < edges_.size(); ++i) {
	    nodes_.push_back(edges_[i].first);
	    nodes_.push_back(edges_[i].second);
	}

	std::sort(nodes_.begin(), nodes_.end());
	nodes_.erase(std::unique(nodes_.begin(), nodes_.end()), nodes_.end());

	// sort edges
	// std::sort(edges_.begin(), edges_.end());

	return true;
    } // read

    void remove(unsigned int id) {
	edges_.erase(std::remove_if(edges_.begin(), edges_.end(), is_adjacent(id)),
		     edges_.end());
	nodes_.erase(std::remove(nodes_.begin(), nodes_.end(), id), nodes_.end());
    } // remove

    template <typename EdgeIter>
    unsigned int gamma(EdgeIter first, EdgeIter last) const {
	std::vector<unsigned int> nodes;

	// get nodes from update
	for (; first != last; ++first) {
	    nodes.push_back(first->first);
	    nodes.push_back(first->second);
	}

	// get nodes from local edges_
	for (unsigned int i = 0; i < edges_.size(); ++i) {
	    nodes.push_back(edges_[i].first);
	    nodes.push_back(edges_[i].second);
	}

	// count degrees
	std::sort(nodes.begin(), nodes.end());

	unsigned int pos = 0;
	unsigned int S = nodes.size();

	for (unsigned int i = 1; i < nodes.size(); ++i) {
	    if (nodes[pos] != nodes[i]) {
		if ((i - pos) < S) S = (i - pos);
		if (S < 2) return 1;
		pos = i;
	    }
	} // for i

	if ((nodes.size() - pos) < S) S = (nodes.size() - pos);

	return S;
    } // gamma

    template <typename EdgeIter>
    void extend(EdgeIter first, EdgeIter last) {
	std::copy(first, last, std::back_inserter(edges_));
	for (; first != last; ++first) {
	    nodes_.push_back(first->first);
	    nodes_.push_back(first->second);
	}
	std::sort(nodes_.begin(), nodes_.end());
	nodes_.erase(std::unique(nodes_.begin(), nodes_.end()), nodes_.end());
    } // extend


private:
    struct is_adjacent {
	explicit is_adjacent(unsigned int id) : id_(id) { }
	bool operator()(const edge_type& e) const {
	    return ((e.first == id_) || (e.second == id_));
	} // operator()
	unsigned int id_;
    }; // struct is_adjacent

    friend bool operator<(const Cluster& lhs, const Cluster& rhs) {
	return lhs.id_ < rhs.id_;
    } // operator<

    friend std::ostream& operator<<(std::ostream& os, const Cluster& c) {
	if (c.edges_.empty() == false) {
	    os << c.id_ << '\t' << c.edges_[0].first << ':' << c.edges_[0].second;
	    for (unsigned int i = 1; i < c.edges_.size(); ++i) {
		os << ' ' << c.edges_[i].first << ':' << c.edges_[i].second;
	    }
	    os << '\n';
	}
	return os;
    } // operator <<

    uint64_t id_;
    std::vector<unsigned int> nodes_;
    std::vector<edge_type> edges_;

}; // Cluster


bool read_clusters(const std::string& name, std::vector<Cluster>& clusters) {
    std::vector<std::string> files;
    if (jaz::dir_list(name, std::back_inserter(files)) == false) return false;

    Cluster c;
    std::ifstream f;

    for (unsigned int i = 0; i < files.size(); ++i) {
	f.open((name + '/' + files[i]).c_str());
	while (!f.eof()) {
	    if (c.read(f) == true) clusters.push_back(c);
	    else break;
	}
	f.close();
    } // for i

    return true;
} // read_clusters

#endif // CLUSTER_HPP
