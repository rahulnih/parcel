# PARCel

PARtition CLustering (PARCeL) is a tool to convert soft clusters into dense hard clusters. This problem is NP-Hard and the tool implements an approximate algorithm.

## Getting Started

This repo provides the source code and some benchmarking examples.

### Prerequisites

C++ 4.8 or higher

### Installing

Run "make"

### Running

parcel <input directory> <output_file>

###

```
* Input directory contains multiple input files. Each line in an input file is information about one soft cluster in input dataset. 
```

```
* The input line contains a cluster number followed by the list of edges in that cluster. See benchmarking datasets for examples.
```

```
* Output file contains the hard clusters produced by parcel. The format of output file is the same is input files.
```

### Example

./parcel ../benchmark/bench1/ out.txt


## Additional notes

Parcel was originally designed as a next step in the analysis pipeline of [Elastic](https://cse.buffalo.edu/~jzola/elastic/). However it can be used to analyze the soft clustering output of any tool.

## Authors

* [Rahul Nihalani](https://rahulnih.bitbucket.io)
* [Jaroslaw Zola](https://cse.buffalo.edu/~jzola/)
